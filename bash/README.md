# Bash & Co

| besoin | ça va fonctionner .... | .... mais on fera plutôt ça |
| --- | --- | --- |
| Supprimer les espaces d'une variable |  `echo ${var} | sed 's/[[:blank:]]//g'` |  `echo ${var//[[:blank:]]/}` |
| Majuscule vers minuscle | `echo ${var} | tr '[[:upper:]]' '[[:lower:]]'` | `echo ${var,,}` |

