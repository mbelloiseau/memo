# Restic

* Timestamp du dernier snapshot

```bash
restic snapshots --last --json \
	jq '.[-1]|.time|strptime("%Y-%m-%dT%H:%M:%S.%Z")|mktime'
```
