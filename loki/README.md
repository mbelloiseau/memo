# Loki, Promtail, Logcli

> Explo Loki et Promtail

On récupère les [3 binaires](https://github.com/grafana/loki/releases/tag/v2.0.0) et un [fichier de configuration Loki](https://raw.githubusercontent.com/grafana/loki/master/cmd/loki/loki-local-config.yaml).

## Loki

```bash
loki -config.file loki-local-config.yml
```

## Promtail

Pour commencer on peut simplement envoyer quelques données via le terminal et sans démarrer de Promtail comme service (`-stdin -server.disable`)

```bash
echo "Ceci est un test" | ./promtail-linux-amd64 -stdin -server.disable \
	-client.url http://localhost:3100/loki/api/v1/push \
	-client.external-labels=lb1=v1,lb2=v2 \
```

## Logcli

`logcli` permet de parcourir et requêter les données dans Loki.

```bash
logcli labels job
logcli labels hostname
logcli query '{hostname="foo"}'
logcli query '{hostname="stdin"}'
```

## Grafana

On dispose de logs "natifs" Nginx, pas de format particulier, pas de JSON ou autre.

```
a.b.c.d - - [17/May/2020:09:51:29 +0200] "GET / HTTP/1.1" 200 612 "-" "Go-http-client/1.1"
```

On peut, par exemple, directement extraire le code HTTP ($status) et le nombre de bytes ($bytes_sent)

```
{filename="/var/log/nginx/access.log"} | regexp "HTTP\\/1\\.1\" (?P<status>[0-9]{3}) (?P<bytes>[0-9]+)"
[...] | regexp "^(?P<remote_ip>\\d+\\.\\d+\\.\\d+\\.\\d+) "
```

Et filtrer à la volée

```
[...] | regexp "HTTP\\/1\\.1\" (?P<status>[0-9]{3}) (?P<bytes>[0-9]+)" | status == 200
[...] | regexp "HTTP\\/1\\.1\" (?P<status>[0-9]{3}) (?P<bytes>[0-9]+)" | status == 200 and bytes > 450
```

## Ressources

* https://grafana.com/docs/loki/latest/installation/local/
