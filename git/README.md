# Git

* Restaurer un fichier

```bash
git log --oneline				# on cherche le commit qui nous intéresse
git checkout c273f87 -- .gitlab-ci.yml		# on restaure le fichier
```

# Gitlab CI

## API

* Etat d'un pipeline et lister les jobs

```bash
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.foo.tld/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.foo.tld/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs"
```

* Télécharger des artifacts

```bash
curl --location --output artifacts.zip --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.foo.tld/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/master/download?job=build_1"
```

A creuser : impossible de récupérer les artifacts d'un tag ?
