# Ansible

## Jinja


```
{% if 'ops' in group_names %}
...
{% else %}
...
{% endif %}
```

## Vérifier les informations de connexion MySQL

Vérifier les informations de connexion MySQL. Dans l'exemple ci-dessous on se connecte toujours sur la même base de données et avec le même utilisateur.

On utilise le module [mysql_query](https://docs.ansible.com/ansible/latest/collections/community/mysql/mysql_query_module.html).

```bash
ansible-galaxy collection install community.mysql
```

Dans le cas présent on s'assure simplement que le mot de passe dans l'inventaire est le bon, le reste des informations est fixe (serveur, nom de la base, utilisateur).

```yaml
- name: "Check MySQL credentials"
      community.mysql.mysql_query:
        login_db: "db_name"
        login_user: "db_user"
        login_password: "{{ mysql_password }}"
        query: SELECT 'OK'
```

## Ressources

* https://ansible-cmdb.readthedocs.io/en/latest/
