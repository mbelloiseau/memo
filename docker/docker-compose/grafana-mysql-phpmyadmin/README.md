# Grafana, MySQL, phpMyAdmin

* Utilisation

Mettez à jour, si besoin, les variables d'environnement ainsi que le réseau utilisé.

```
docker-compose up -d
docker-compose logs -f
```

* URLs
  * Grafana : http://localhost:3000
  * phpMyAdmin : http://localhost:8080
