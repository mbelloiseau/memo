# Mémo

Un peu de tout, des bouts de conf, des scripts, des liens, des astuces ... pour les retrouver facilement et pour partager :wink:

* Ansible
  * [Vérifier ses informations de connexion MySQL]((./ansible/README.md))
* Docker
  * [Grafana, MySQL, phpMyAdmin](./docker/docker-compose/grafana-mysql-phpmyadmin)
* Nginx
  * [Quelques tips](./nginx/README.md)
* Systemd
  * [Définition de service](./systemd/config)
* Loki
  * [Explo Loki, Promtail, Logcli](./loki/README.md)
* Sed, Awk, Bash, ...
  * [Bash tips](./bash/README.md)
