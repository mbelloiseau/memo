# Nginx

* Forcer HTTPS

```
if ($scheme != "https") {
    return 301 https://$host$request_uri;
}
```

* On coupe court si **$host** n'est pas celui attendu

```
if ($host !~ ^(foo.domain.tld|...|...)$ ) {
	return 444;
}
```

> 444 Connection Closed Without Response
> A non-standard status code used to instruct nginx to close the connection without sending a response to the client, most commonly used to deny malicious or malformed requests.

On peut ensuite vérifier le comportement avec `curl`

```
curl -s -o /dev/null -w "%{http_code}" http://foo.domain.tld
```
