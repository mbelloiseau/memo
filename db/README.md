# DB

## MySQL

```bash
mysql --user=user --password=strong_password -e "DROP DATABASE db;"
mysql --user=user --password=strong_password -e "CREATE DATABASE db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
```

* Tester la connexion

```bash
echo "SELECT 'OK';" | mysql --user=root --password="db" --host=db db
```
