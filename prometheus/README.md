# Prometheus

## S'assurer de la présence d'une métrique

```yaml
groups:
- name: missing metrics
  rules:
  - alert: restic_check metric is missing !
    expr: up{job="production"} == 1 unless on(instance) restic_check{job="production"}
    for: 1m
```

## PromQL

### `count_values`

Exemple avec les codes http

```
count_values("code", probe_http_status_code)
```

### `count` 

```
# soit une métrique
pkg_version{name="php",version="7.2"} 1
# query
sum by (version) (pkg_version{name="php"})
```
